#!/usr/bin/env python3

###
# This program is my solution to the get-in-it coding challenge (https://www.get-in-it.de/coding-challenge).
# It finds the "shortest" way from the "planet" Earth to "b3-r7-r4nd7". The planets are the nodes of an (undirected) graph.
# It is only possible to navigate from one planet to another on the edges of the graph. The "shortest" way is the way with the lowest total cost of all edges included.
# This program uses the Dijkstra algorithm to solve the task
###

# A planet object has a name/label, a distance to the first planet and a predecessor
# The predecessor is the node in the graph which comes before this one in the shortest path from the start we found so far
# Both the predecessor and the distance are subject to updates
class planet:
    def __init__(self,mylabel):
        self.distance=float("inf")
        self.name=mylabel
        self.predecessor=None

# return the index of the next node to "visit" (=investigate), which is the one with the shortest distance to the start that hasn't been visited yet
def next_node(nodes):
    shortest_dist=float("inf")
    for key, value in nodes.items():
        if value.distance<shortest_dist:
            shortest_dist=value.distance
            next_node=key
    return next_node

# Investigate a planet & update distances: Find all planets that are "neighbours" to this one.
# If they haven't been visited yet, it could be that we found a shorter way to those planet.
# Check that! If so, update the distance & the predecessor (now this planet)
def update(myindex,myedges,unvisited):
    for edge in myedges: # Loop over all edges
        if edge['source']==myindex and edge['target'] in unvisited:
        # Found a way starting at this planet to a new one
            if unvisited[myindex].distance+edge['cost']<unvisited[edge['target']].distance:
            # ...and the path is shorter than the existing one!
                unvisited[edge['target']].distance=unvisited[myindex].distance+edge['cost']
                unvisited[edge['target']].predecessor=myindex
                # Updated distance & predecessor
        if edge['target']==myindex and edge['source'] in unvisited:
        # Found a way ending at this planet from a new one
            if unvisited[myindex].distance+edge['cost']<unvisited[edge['source']].distance:
            # ...and the path is shorter than the existing one!
                unvisited[edge['source']].distance=unvisited[myindex].distance+edge['cost']
                unvisited[edge['source']].predecessor=myindex
                # Updated distance & predecessor

# This routine concatenates the names of all planets and their predecessors recurively from
# the destination to the start to print out the solution
def finalize_recursive(visited,rec_dest):
    if visited[rec_dest].predecessor is None: # Reached the start recursively
        return visited[rec_dest].name
    return finalize_recursive(visited,visited[rec_dest].predecessor)+' -> '+visited[rec_dest].name
#############################################################
# MAIN program HERE!
#############################################################        
# first, load json file
import json
with open('generatedGraph.json') as json_file:
    mydata = json.load(json_file)
    nodes=mydata['nodes']
    edges=mydata['edges']
    # initializing dicts, start and destination...
    to_visit={}
    visited={}
    start='Erde'
    dest='b3-r7-r4nd7'
    # set up the dict with unvisited planets (at the beginning: all...)
    for i,n in enumerate(nodes):
        myplanet=planet(n['label'])
        if (n['label']=='Erde'):
            myplanet.distance=0.0
        to_visit[i]=myplanet
    # now, loop over all planets until the destination has been visited
    # Note: If a planet currently has the shortest distance of all unvisited planets, we cannot possibly find a new path with a shorter distance to this planet since we do not have negative costs for any path.
    while(len(to_visit)>0):
        # always chose planet with shortest distance first (see above)
        nextnode=next_node(to_visit)
        if to_visit[nextnode].name==dest: # We already found the destination!
            visited[nextnode]=to_visit.pop(nextnode) # Declare it visited (necessary for finalize_recursive)
            print('The shortest path is:')
            print(finalize_recursive(visited,nextnode)) # Print path
            print('and it has a total cost/distance of: ')
            print(visited[nextnode].distance) # Print distance
            raise SystemExit
        update(nextnode,edges,to_visit) # Update distances
        visited[nextnode]=to_visit.pop(nextnode) # This node is "closed" now. We do not need to investigate it any more
